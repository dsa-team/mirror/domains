; ds-in-parent = yes

$TTL 8h

@		IN	NS	dns4.easydns.info.
@		IN	NS	sec1.rcode0.net.
@		IN	NS	sec2.rcode0.net.
@		IN	NS	nsp.dnsnode.net.

$TTL 8h

@		IN	MX	10 mta-gw.infomaniak.ch.
		IN	A	104.198.14.52

buzz		IN	A	46.227.224.179
		IN	AAAA	2a02:cd8:abac:27::33

www		IN	CNAME	debian-ch.netlify.com.

; vim: syn=dns:
