; ds-in-parent = yes

$TTL 8h

@		IN	NS	dns4.easydns.info.
@		IN	NS	sec1.rcode0.net.
@		IN	NS	sec2.rcode0.net.
@		IN	NS	nsp.dnsnode.net.

$TTL 8h

@		IN	MX	10 mailly.debian.org.
@		IN	MX	10 mitropoulos.debian.org.
@		IN	MX	10 muffat.debian.org.

rns2			IN	A	176.97.158.100
			IN	AAAA	2001:67c:10b8::100
			IN	TXT	"sec2.rcode0.net"
rns4			IN	A	64.68.197.10
			IN	AAAA	2620:49:4::10
			IN	TXT	"dns4.easydns.info"

@               IN      CAA     128 issue ";"
@               IN      CAA     0 iodef "mailto:dsa@debian.org"

; A records for @, from www
$INCLUDE "/srv/dns.debian.org/var/services-auto/all"

www		IN	CNAME	www.debian.org.

; vim: syn=dns:
