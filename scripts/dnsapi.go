package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"

	"github.com/goccy/go-yaml"
	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "dnsapi",
	Short: "Updates DNS serving infrastructure",
}

type NetnodAPI struct {
	Endpoint string
	Token    string
}

type NetnodMasterEntry struct {
	IP   string `json:"ip"`
	TSIG string `json:"tsig"`
}

type NetnodZone struct {
	Masters []*NetnodMasterEntry `json:"masters"`
}

func (a *NetnodAPI) UpdateMasters(zone string, ips []string, tsigKey string) error {
	z := &NetnodZone{}
	for _, ip := range ips {
		z.Masters = append(z.Masters, &NetnodMasterEntry{
			IP:   ip,
			TSIG: tsigKey,
		})
	}
	b, err := json.Marshal(z)
	if err != nil {
		return fmt.Errorf("could not marshal JSON: %w", err)
	}
	u, err := url.JoinPath(a.Endpoint, "/zone/", zone, "/")
	if err != nil {
		return fmt.Errorf("could not construct URL: %w", err)
	}
	req, err := http.NewRequest(http.MethodPatch, u, bytes.NewReader(b))
	if err != nil {
		return fmt.Errorf("could not construct request to %q with payload %q: %w", u, string(b), err)
	}
	req.Header.Add("Authorization", fmt.Sprintf("Token %s", a.Token))
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Errorf("could not send request to Netnod: %w", err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusNoContent {
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			return fmt.Errorf("could not read response body for error %s: %w", resp.Status, err)
		}
		return fmt.Errorf("error %s from Netnod: %s", resp.Status, body)
	}
	return nil
}

type RcodeZeroAPI struct {
	Endpoint string
	Token    string
}

type RcodeZeroZone struct {
	Masters []string `json:"masters"`
}

func (a *RcodeZeroAPI) UpdateMasters(zone string, ips []string) error {
	z := &RcodeZeroZone{}
	for _, ip := range ips {
		z.Masters = append(z.Masters, ip)
	}
	b, err := json.Marshal(z)
	if err != nil {
		return fmt.Errorf("could not marshal JSON: %w", err)
	}
	u, err := url.JoinPath(a.Endpoint, "/zones/", zone)
	if err != nil {
		return fmt.Errorf("could not construct URL: %w", err)
	}
	req, err := http.NewRequest(http.MethodPut, u, bytes.NewReader(b))
	if err != nil {
		return fmt.Errorf("could not construct request to %q with payload %q: %w", u, string(b), err)
	}
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", a.Token))
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Errorf("could not send request to RcodeZero: %w", err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			return fmt.Errorf("could not read response body for error %s: %w", resp.Status, err)
		}
		return fmt.Errorf("error %s from RcodeZero: %s", resp.Status, body)
	}
	return nil
}

type EasyDNSAPI struct {
	Endpoint string
	Token    string
	Key      string
}

func (a *EasyDNSAPI) UpdateMaster(zone string, ip string) error {
	var z struct {
		Master string `json:"master"`
	}
	z.Master = ip
	b, err := json.Marshal(z)
	if err != nil {
		return fmt.Errorf("could not marshal JSON: %w", err)
	}
	u, err := url.JoinPath(a.Endpoint, "/domains/primary_ns/", zone)
	if err != nil {
		return fmt.Errorf("could not construct URL: %w", err)
	}
	req, err := http.NewRequest(http.MethodPost, u, bytes.NewReader(b))
	if err != nil {
		return fmt.Errorf("could not construct request to %q with payload %q: %w", u, string(b), err)
	}
	req.SetBasicAuth(a.Token, a.Key)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Errorf("could not send request to EasyDNS: %w", err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			return fmt.Errorf("could not read response body for error %s: %w", resp.Status, err)
		}
		return fmt.Errorf("error %s from EasyDNS: %s", resp.Status, body)
	}
	return nil
}

var updatePrimaryCmd = &cobra.Command{
	Use:   "update-primary",
	Short: "Update primary NS set with the providers",
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		const configFile = "dnsapi.yaml"
		c, err := os.ReadFile(configFile)
		if err != nil {
			return fmt.Errorf("could not read config file %q: %w", configFile, err)
		}
		var apis Config
		if err := yaml.Unmarshal(c, &apis); err != nil {
			return fmt.Errorf("could not unmarshal YAML: %w", err)
		}
		fmt.Printf("%#+v\n", apis)

		zone := args[0]
		if err := apis.Netnod.UpdateMasters(zone, []string{"194.177.211.218", "2001:648:2ffc:deb::211:218"}, "netnod-debian-20171122."); err != nil {
			return fmt.Errorf("failed to update masters for %q: %w", zone, err)
		}
		if err := apis.RcodeZero.UpdateMasters(zone, []string{"194.177.211.218", "2001:648:2ffc:deb::211:218"}); err != nil {
			return fmt.Errorf("failed to update masters for %q: %w", zone, err)
		}
		if err := apis.EasyDNS.UpdateMaster(zone, "194.177.211.219"); err != nil {
			return fmt.Errorf("failed to update masters for %q: %w", zone, err)
		}
		return nil
	},
}

type Config struct {
	Netnod    NetnodAPI    `yaml:"Netnod"`
	RcodeZero RcodeZeroAPI `yaml:"RcodeZero"`
	EasyDNS   EasyDNSAPI   `yaml:"EasyDNS"`
}

func init() {
	rootCmd.AddCommand(updatePrimaryCmd)
}

func main() {
	rootCmd.Execute()
}
