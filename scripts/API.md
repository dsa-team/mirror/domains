## Netnod

PATCH `/zone/<zone>/`

```
{
  "masters": [
    {
      "ip": "<ip1>",
      "tsig": "<tsig-key-name>"
    },
    {
      "ip": "<ip2>",
      "tsig": "<tsig-key-name>"
    }
  ]
}

TSIG key: netnod-debian-20171122.

## RcodeZero

PUT `/api/v2/zones/<zone>`

```
{
  "masters": ["<ip1>", "<ip2>"]
}
```

## EasyDNS

POST `/domains/primary_ns/<zone>`

```
{
  "master": "<ip>"
}
```


