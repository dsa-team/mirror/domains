; ds-in-parent = no

$TTL 8h

@		IN	NS	sec1.rcode0.net.
@		IN	NS	sec2.rcode0.net.
@		IN	NS	nsp.dnsnode.net.
@		IN	NS	dns4.easydns.info.

$TTL 8h

@		IN	MX	10 mailly.debian.org.
@		IN	MX	10 mitropoulos.debian.org.
@		IN	MX	10 muffat.debian.org.

@               IN      CAA     128 issue ";"
@               IN      CAA     0 iodef "mailto:dsa@debian.org"

; A records for @, from www
$INCLUDE "/srv/dns.debian.org/var/services-auto/all"

www		IN	CNAME	www.debian.org.

; vim: syn=dns:
