; ds-in-parent = no

$TTL 8h

@		IN	NS	sec1.rcode0.net.
@		IN	NS	sec2.rcode0.net.
@		IN	NS	nsp.dnsnode.net.
;@		IN	NS	dns4.easydns.info.

$TTL 8h

;@		IN	TXT	google-site-verification=5Rfrh4oYkRbVAUB7hjh-JhvOo5VGOf7GOLYGakhaUk4

@		IN	CAA	0 iodef "mailto:dsa@debian.org"
@		IN	CAA	128 issue "letsencrypt.org;validationmethods=dns-01;accounturi=https://acme-v02.api.letsencrypt.org/acme/acct/346607"
@		IN	CAA	128 issuewild ";"

@		IN	MX	0 .

; A records for @, from static
$INCLUDE "/srv/dns.debian.org/var/services-auto/all"

www		IN	CNAME	static.debian.org.

; so we can get certificates
$INCLUDE "/srv/letsencrypt.debian.org/var/hook/snippet"
$INCLUDE "/srv/dehydrated/var/hook/snippet"

; vim: syn=dns:
